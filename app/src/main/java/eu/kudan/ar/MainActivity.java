package eu.kudan.ar;

import android.os.Bundle;

import eu.kudan.kudan.*;
import eu.kudan.kudan.ARAPIKey;
import eu.kudan.kudan.ARActivity;
import eu.kudan.kudan.ARImageTrackable;
import eu.kudan.kudan.ARImageTracker;
import eu.kudan.kudan.ARLightMaterial;
import eu.kudan.kudan.ARMeshNode;
import eu.kudan.kudan.ARModelImporter;
import eu.kudan.kudan.ARModelNode;
import eu.kudan.kudan.ARTexture2D;

public class MainActivity extends ARActivity {

    ARImageTrackable trackable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_main);

        //Setup API KEY
        ARAPIKey key = ARAPIKey.getInstance();
        key.setAPIKey("GAWQE-F9AQU-2G87F-8HKED-Q7BTG-TY29G-RV85A-XN3ZP-A9KGM-E8LB6-VC2XW-VTKAK-ANJLG-2P8NX-UZMAH-Q");
    }

    @Override
    public void setup() {
        super.setup();

        trackable = new ARImageTrackable("image1");
        trackable.loadFromAsset("p.jpg");

        ARImageTracker tManager = ARImageTracker.getInstance();
        tManager.addTrackable(trackable);

        setupModel();
    }

    public void setupModel() {
        ARModelImporter mImporter = new ARModelImporter();
        mImporter.loadFromAsset("TV_animTest.jet");
        ARModelNode mNode = (ARModelNode) mImporter.getNode();

        ARTexture2D texture2D = new ARTexture2D();
        texture2D.loadFromAsset("tex.jpg");

        ARLightMaterial material = new ARLightMaterial();
        material.setTexture(texture2D);
        material.setAmbient(0.8f, 0.8f, 0.8f);

        // Apply texture material to models mesh nodes
        for(ARMeshNode meshNode : mImporter.getMeshNodes()){
            meshNode.setMaterial(material);
        }

        mNode.scaleByUniform(400f);
        mNode.rotateByDegrees(45, 1, 0, 0);
        mNode.play();

        trackable.getWorld().addChild(mNode);
    }

    public void setupImage() {
        ARImageNode imageNode = new ARImageNode("spaceMarker.jpg");
        trackable.getWorld().addChild(imageNode);
    }

}
